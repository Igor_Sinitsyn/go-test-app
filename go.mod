module gitlab.com/ms-ural/airport/comman/backend

go 1.16

require (
	github.com/Nerzal/gocloak/v8 v8.1.1
	github.com/bradfitz/gomemcache v0.0.0-20190913173617-a41fca850d0b
	github.com/dgrijalva/jwt-go/v4 v4.0.0-preview1
	github.com/go-resty/resty/v2 v2.4.0 // indirect
	github.com/gofrs/uuid v4.0.0+incompatible // indirect
	github.com/google/go-cmp v0.5.4 // indirect
	github.com/google/uuid v1.0.0
	github.com/gorilla/mux v1.8.0
	github.com/jackc/pgmock v0.0.0-20201204152224-4fe30f7445fd // indirect
	github.com/jackc/pgproto3/v2 v2.0.7 // indirect
	github.com/jackc/pgtype v1.7.0
	github.com/jackc/pgx/v4 v4.11.0
	github.com/lib/pq v1.9.0 // indirect
	github.com/prometheus/client_golang v1.9.0
	github.com/prometheus/procfs v0.3.0 // indirect
	github.com/rs/cors v1.7.0
	github.com/shopspring/decimal v1.2.0 // indirect
	gitlab.com/ms-ural/airport/core/logger.git v0.0.3
	gitlab.com/ms-ural/airport/core/master-data.git v0.0.1
	go.uber.org/zap v1.18.1
	golang.org/x/crypto v0.0.0-20210322153248-0c34fe9e7dc2
	golang.org/x/lint v0.0.0-20201208152925-83fdc39ff7b5 // indirect
	golang.org/x/sys v0.0.0-20210124154548-22da62e12c0c // indirect
	golang.org/x/text v0.3.5 // indirect
	golang.org/x/tools v0.1.0 // indirect
	google.golang.org/protobuf v1.25.0 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
)

//replace gitlab.com/ms-ural/airport/core/master-data.git => ./../../core/master-data
