package main

import (
	"errors"
	"strconv"
	"strings"

	"github.com/dgrijalva/jwt-go/v4"
	"gitlab.com/ms-ural/airport/core/logger.git"
)

type folder struct {
	ID       int32    `json:"id"`
	Name     string   `json:"name"`
	Unread   int64    `json:"unread"`
	BaseLink string   `json:"baseLink"`
	Filters  []folder `json:"filters"`
	Mode     string   `json:"mode"`
}

type commanUserFields struct {
	baseAps []int32
	folders []folder
}

func canExec(ui logger.User, folder int32) (bool, error) {
	if ui.Data != nil {
		for _, val := range ui.Data.(commanUserFields).folders {
			if val.ID == folder {
				if val.Mode == "full" {
					return true, nil
				} else {
					return false, nil
				}
			}
		}
	} else {
		return false, errors.New("commanUserFields not set")
	}

	return false, errors.New("no access to folder")
}

func getUserInfo(claims *jwt.MapClaims) (logger.User, error) {
	ui := logger.User{}

	cus := commanUserFields{}
	// BASE APS
	{
		temp, ok := map[string]interface{}(*claims)["baseAps"]
		if ok {
			for _, item := range temp.([]interface{}) {
				cus.baseAps = append(cus.baseAps, int32(item.(float64)))
			}
		}
	}

	// FOLDERS
	{
		temp, ok := map[string]interface{}(*claims)["folders"]
		if ok {
			for _, item := range temp.([]interface{}) {
				t := strings.Split(item.(string), ";;;") // id;;;name;;;mode
				if len(t) == 3 {
					id, err := strconv.Atoi(t[0])
					if err != nil {
						return logger.User{}, errors.New("invalid folders structure: " + item.(string))
					}
					cus.folders = append(cus.folders, folder{ID: int32(id), Mode: t[2]})
				}
			}
		}
	}

	// USERNAME
	{
		user, ok := map[string]interface{}(*claims)["preferred_username"]
		if !ok {
			return logger.User{}, errors.New("token doesn't contain preferred_username")
		}

		ui.Name = user.(string)
	}

	if len(cus.baseAps) == 0 && len(cus.folders) != 0 {
		return logger.User{}, errors.New("invalid settings: no base aps with folders")
	}

	ui.Data = cus

	return ui, nil
}
