package main

import (
	"context"
	"errors"
	"os"
	"strconv"
	"time"

	"net/http"

	"github.com/Nerzal/gocloak/v8"
	"github.com/bradfitz/gomemcache/memcache"
	"github.com/gorilla/mux"
	"gitlab.com/ms-ural/airport/core/logger.git"
	"golang.org/x/crypto/acme/autocert"

	"github.com/jackc/pgx/v4/pgxpool"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/rs/cors"

	"log"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"

	masterdata "gitlab.com/ms-ural/airport/core/master-data.git"
)

const (
	// Product - event.module in ECS
	Product string = "comman"
	// Component - event.provider in ECS
	Component string = "backend"
)

var (
	dbFast                                          *pgxpool.Pool
	dbSlow                                          *pgxpool.Pool
	msu                                             *logger.MsuLogger
	databaseErrors                                  prometheus.Counter
	getDurations                                    *prometheus.HistogramVec
	baseAp                                          int
	language                                        string
	mc                                              *memcache.Client
	commanReceiverEndpoint                          string
	client                                          gocloak.GoCloak
	keyCloakHostname, clientID, clientSecret, realm string
	databusEndpoint                                 string
	httpsEnabled                                    = true
	timeout                                         = 15
	md                                              *masterdata.Client
)

func main() {
	var err error

	cfg := zap.Config{
		Encoding:         "json",
		Level:            zap.NewAtomicLevelAt(zapcore.DebugLevel),
		OutputPaths:      []string{"stderr"},
		ErrorOutputPaths: []string{"stderr"},
		EncoderConfig:    zap.NewProductionEncoderConfig(),
		Sampling: &zap.SamplingConfig{
			Initial:    100,
			Thereafter: 100,
		},
		Development: false,
	}

	zaplogger, err := cfg.Build(zap.AddCaller(), zap.AddCallerSkip(1))

	if err != nil {
		log.Fatal(err)
	}
	defer zaplogger.Sync()

	msu = logger.NewMsuLogger(zaplogger, Product, Component)

	/** PROMETHEUS */
	/* Database errors counter */
	databaseErrors = prometheus.NewCounter(
		prometheus.CounterOpts{
			Name: "database_errors",
		})
	prometheus.MustRegister(databaseErrors)
	getDurations = prometheus.NewHistogramVec(prometheus.HistogramOpts{
		Name:    "get_durations_seconds",
		Help:    "duration histogram",
		Buckets: []float64{0.05, 0.1, 0.25, 0.75, 1, 2},
	},
		[]string{"api"})
	prometheus.MustRegister(getDurations)

	if ba, ok := os.LookupEnv("BASE_AP"); ok {
		var temp int
		var err error
		if temp, err = strconv.Atoi(ba); err != nil {
			msu.Error(context.Background(), err, zap.String("BASE_AP", ba))
		}

		baseAp = temp
	} else {
		baseAp = 1
	}

	if l, ok := os.LookupEnv("LANGUAGE"); ok {
		var err error
		if _, err = strconv.Atoi(l); err != nil {
			msu.Error(context.Background(), errors.New("variable LANGUAGE is not valid int"), zap.String("LANGUAGE", l))
		}

		language = l
	} else {
		language = "1"
	}

	var ok bool
	if keyCloakHostname, ok = os.LookupEnv("KEYCLOAK_HOST"); !ok {
		keyCloakHostname = "http://dev.msural.ru:8080"
	}

	clientID = "comman"
	if clientSecret, ok = os.LookupEnv("SECRET"); !ok {
		clientSecret = "a24fed5c-8318-4793-90ad-795fc226613f"
	}

	if realm, ok = os.LookupEnv("REALM"); !ok {
		realm = "msu"
	}

	if _, ok = os.LookupEnv("HTTPS_DISABLED"); ok {
		httpsEnabled = false
	}

	msu.Info(context.Background(), zap.Any("config", cfg), zap.Int("BASE_AP", baseAp), zap.String("LANGUAGE", language))

	/** POSTGRESQL */
	var username, password, hosts, dbname string
	if username, ok = os.LookupEnv("DB_USERNAME"); !ok {
		username = "postgres"
	}
	if password, ok = os.LookupEnv("DB_PASSWORD"); !ok {
		password = "raspberrypi2017"
	}
	if hosts, ok = os.LookupEnv("DB_HOSTS"); !ok {
		hosts = "dev.msural.ru:5433"
	}
	if dbname, ok = os.LookupEnv("DB_NAME"); !ok {
		dbname = "aodb"
	}
	// POSTGRES FAST QUERY
	{
		connection := "postgres://" + username + ":" + password + "@" + hosts + "/" + dbname + "?target_session_attrs=read-write"
		poolConfig, err := pgxpool.ParseConfig(connection)
		if err != nil {
			msu.Fatal(context.Background(), err)
		}
		poolConfig.ConnConfig.Logger = logger.NewLogger(zaplogger)
		poolConfig.MaxConns = 20
		poolConfig.MaxConnLifetime = time.Hour
		poolConfig.HealthCheckPeriod = time.Minute

		dbFast, err = pgxpool.ConnectConfig(context.Background(), poolConfig)
		if err != nil {
			msu.Fatal(context.Background(), err)
		}
	}

	client = gocloak.NewClient(keyCloakHostname)

	_, err = client.LoginClient(context.Background(), clientID, clientSecret, realm)
	if err != nil {
		msu.Fatal(context.Background(), err, zap.String("keycloak", keyCloakHostname+"/"+realm))
	}
	msu.Info(context.Background(), zap.String("keycloak", keyCloakHostname+"/"+realm))

	if databusEndpoint, ok = os.LookupEnv("DATABUS_ENDPOINT"); !ok {
		msu.Warn(context.Background(), errors.New("databus settings not set"))
	} else {
		msu.Info(context.Background(), zap.String("databus", databusEndpoint))
	}

	/** EndPoint **/

	mc = memcache.New("84.201.156.71:11211", "84.201.156.71:11211", "84.201.156.71:11211")

	md = masterdata.NewClient(msu, dbFast)

	corsOpts := cors.New(cors.Options{
		AllowedOrigins: []string{"*"}, //you service is available and allowed for this base url
		AllowedMethods: []string{
			http.MethodGet,
			http.MethodPost,
			http.MethodPut,
			http.MethodOptions,
		},

		AllowedHeaders: []string{
			"*", //or you can your header key values which you are using in your application

		},
		ExposedHeaders: []string{
			"*",
		},
	})

	if httpsEnabled {
		dir := "/opt/certs"
		hostPolicy := func(ctx context.Context, host string) error {
			return nil
		}
		certManager := autocert.Manager{
			Prompt:     autocert.AcceptTOS,
			HostPolicy: hostPolicy,
			Cache:      autocert.DirCache(dir),
			Email:      "info@msural.ru",
		}

		server := &http.Server{
			Addr:      ":8443",
			Handler:   prometheusHandler(corsOpts.Handler(authorization(handlers()))),
			TLSConfig: certManager.TLSConfig(),
		}

		go func(s *http.Server) {
			err := http.ListenAndServe(":8080", certManager.HTTPHandler(nil))
			if err != nil {
				if e := s.Shutdown(context.Background()); e != nil {
					msu.Fatal(context.Background(), e)
				}
				msu.Fatal(context.Background(), err)
			}
		}(server)

		err = server.ListenAndServeTLS("", "")
	} else {
		//err = http.ListenAndServe(":8080", prometheusHandler(corsOpts.Handler(authorization(handlers()))))
		err = http.ListenAndServe(":8080", corsOpts.Handler(handlers()))
	}

	if err != nil {
		msu.Fatal(context.Background(), err)
	}
}

func handlers() http.Handler {
	r := mux.NewRouter()

	// PROMETHEUS
	r.Handle("/metrics", promhttp.Handler())

	r.HandleFunc("/master-data/{md}", md.SelectRecords).Methods("GET")

	return r
}
