package main

import "encoding/json"

type response struct {
	Type    string `json:"type"`
	Message string `json:"message"`
	Detail  string `json:"detail"`
}

func (r response) Marshal() string {
	var result []byte
	var err error
	if result, err = json.Marshal(r); err != nil {
		return "{\"type\":\"error\",\"message\":\"Internal Server Error\",\"detail\":\"make response error:\"" + err.Error() + "\""
	}

	return string(result)
}
