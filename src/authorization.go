package main

import (
	"context"
	"errors"
	"fmt"
	"net/http"
	"strings"

	"gitlab.com/ms-ural/airport/core/logger.git"
	"go.uber.org/zap"
)

func authorization(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		if r.RequestURI == "/metrics" {
			h.ServeHTTP(w, r)
			return
		}

		ctx := r.Context()

		var token string
		tokenInfo := strings.Split(r.Header.Get("Authorization"), " ")
		if len(tokenInfo) == 2 {
			token = tokenInfo[1]
		}

		rptResult, err := client.RetrospectToken(ctx, token, clientID, clientSecret, realm)
		if err != nil {
			msu.Error(ctx, err)
			w.WriteHeader(http.StatusBadRequest)
			fmt.Fprint(w, response{Type: "error", Message: "TODO", Detail: err.Error()}.Marshal())
			return
		}

		if !(*rptResult.Active) {
			msu.Warn(ctx, errors.New("unauthorized"), zap.String("token", token), zap.Any("rptResult", rptResult))
			w.WriteHeader(http.StatusUnauthorized)
			fmt.Fprint(w, response{Type: "error", Message: "Unathorized  request", Detail: "Invalid or empty authorization header"}.Marshal())
			return
		}

		_, claims, err := client.DecodeAccessToken(ctx, token, realm, "")

		if err != nil {
			msu.Error(ctx, err)
			w.WriteHeader(http.StatusInternalServerError)
			fmt.Fprint(w, response{Type: "error", Message: "TODO", Detail: err.Error()}.Marshal())
			return
		}

		ui, err := getUserInfo(claims)
		if err != nil {
			msu.Error(ctx, err)
			w.WriteHeader(http.StatusInternalServerError)
			fmt.Fprint(w, response{Type: "error", Message: "TODO", Detail: err.Error()}.Marshal())
			return
		}

		ctx = context.WithValue(ctx, logger.UserContextKey, ui)
		h.ServeHTTP(w, r.WithContext(ctx))
	})
}
