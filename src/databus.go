package main

import (
	"bytes"
	"encoding/json"
	"errors"
	"net/http"
	"time"

	"github.com/jackc/pgtype"
	"gitlab.com/ms-ural/airport/core/logger.git"

	"go.uber.org/zap"
)

func pushToDatabus(r *http.Request, messageID int32, flights []int32) error {

	if databusEndpoint == "" {
		return nil
	}

	var m, sender, t pgtype.Varchar
	var dtMessage pgtype.Timestamp
	var direction pgtype.Int4
	var parsed pgtype.JSONB

	ctx := r.Context()
	if err := dbFast.QueryRow(ctx,
		`SELECT 
			messages.message, 
			messages.sender, 
			message_type.name[1], 
			messages.dt_insert, 
			messages.direction, 
			messages.parsed_json 
		 FROM messages.messages
		 INNER JOIN master_data.message_types message_type ON message_type.id = messages.type_id 
		 WHERE messages.id = $1`, messageID).Scan(&m, &sender, &t, &dtMessage, &direction, &parsed); err != nil {
		return err
	}

	mes := struct {
		MessageID  int32     `json:"id"`
		Message    string    `json:"message"`
		Sender     string    `json:"sender"`
		Type       string    `json:"type"`
		Flights    []int32   `json:"flights"`
		DtMessage  time.Time `json:"dtMessage"`
		Direction  int32     `json:"direction"`
		XRequestID string    `json:"requestId"`
		Parsed     []byte    `json:"parsed"`
	}{
		MessageID:  messageID,
		Message:    m.String,
		Sender:     sender.String,
		Type:       t.String,
		Flights:    flights,
		DtMessage:  dtMessage.Time,
		Direction:  direction.Int,
		XRequestID: r.Context().Value(logger.RequestURIContextKey).(string),
		Parsed:     parsed.Bytes,
	}

	body, err := json.Marshal(mes)
	if err != nil {
		return err
	}

	req, err := http.NewRequest("POST", databusEndpoint+"/messages", bytes.NewBuffer(body))
	if err != nil {
		return err
	}
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("X-Request-ID", ctx.Value(logger.TraceIDContextKey).(string))
	req.Header.Set("X-Correlation-ID", ctx.Value(logger.SessionIDContextKey).(string))

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode == 201 || resp.StatusCode == 200 {
		msu.Info(ctx, zap.String("endpoint", databusEndpoint+"/messages"), zap.ByteString("body", body), zap.String("respStatus", resp.Status))
	} else {
		msu.Error(ctx, errors.New("request failed"), zap.String("endpoint", databusEndpoint+"/messages"), zap.ByteString("body", body), zap.String("respStatus", resp.Status))
		return errors.New("request failed")
	}

	return nil
}
